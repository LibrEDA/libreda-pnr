<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# LibrEDA Place-and-Route

libreda-pnr defines the concepts of placement and routing algorithms for the LibrEDA framework.
This includes mainly trait definitions for place and route algorithms. Implementations following this traits
can easily be used together with the rest of the framework.

# Implementations

Implementations of place and route algorithms are not included in this crate.
Some can be found in the following list:

## Placement and legalizaton
* [electron-placer](https://codeberg.org/LibrEDA/electron-placer) (written in Rust)
* [Coloquinte](https://codeberg.org/tok/libreda-coloquinte-placer) (C bindings, statically linked)

## Routing
* [TritonRoute](https://codeberg.org/LibrEDA/libreda-triton-route) (subprocess called by passing LEF/DEF files)
* [mycelium](https://codeberg.org/LibrEDA/mycelium-router) (written in Rust)
