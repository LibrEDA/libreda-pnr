// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Representation of the solution of a placement problem.
//!
//! A trait based representation gives more flexibility.

use crate::place::placement_problem::PlacementStatus;
pub use libreda_db::prelude as db;
use std::collections::HashMap;

/// Representation of the placement solution.
pub trait PlacementSolution<C: db::LayoutBase> {
    /// Get the positions of the placed cell instances.
    fn get_positions(&self) -> HashMap<C::CellInstId, db::SimpleTransform<C::Coord>>;

    /// Get the placement status of the instance.
    fn placement_status(&self, cell_instance: &C::CellInstId) -> PlacementStatus;
}
