// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Basic traits for a detail router.

use crate::db;
use crate::route::pin_access_oracle::PinAccessOracle;
use crate::route::routing_problem::DetailRoutingProblem;

/// Basic trait for a detail router.
///
/// A router which follows the concept of this trait takes as input the description of the routing problem (placed layout, nets to be routed, etc.)
/// and computes the routes which connect the nets to their pins. The router is not allowed to modify the layout nor the netlist but instead it returns
/// a struct which encodes the result of the detail routing. The `RoutingResult` must implement [`DrawDetailRoutes`] which allows then to apply the
/// routing result to the layout, e.g. draw the wires and vias.
/// On failure the router should return an error.
pub trait DetailRouter<LN: db::L2NBase> {
    /// Result of the detail routing.
    /// The exact type depends on the routing algorithm. The only constraint is
    /// that the result can be drawn to a layout.
    type RoutingResult: DrawDetailRoutes<LN>;
    /// Failure during routing.
    type Error;

    /// Get the name of the routing engine.
    fn name(&self) -> String;

    /// Compute the detail routes.
    /// Neither layout nor netlist are modified. However, a successful result can
    /// be written to the layout.
    fn route<RP>(&self, routing_problem: RP) -> Result<Self::RoutingResult, Self::Error>
    where
        RP: DetailRoutingProblem<LN> + Sync;

    /// Compute the detail routes with provided pin access locations.
    /// Neither layout nor netlist are modified.
    fn route_with_pin_access_oracle(
        &self,
        routing_problem: impl DetailRoutingProblem<LN> + Sync,
        _pin_access_oracle: &impl PinAccessOracle<LN>,
    ) -> Result<Self::RoutingResult, Self::Error> {
        // The default implementation ignores the oracle.
        self.route(routing_problem)
    }
}

/// Trait for routing results which can be drawn to a layout.
pub trait DrawDetailRoutes<LN: db::L2NBase> {
    /// Failure during drawing of the routes.
    type Error;
    /// Draw the detail routes to a layout/netlist.
    fn draw_detail_routes(&self, chip: &mut LN, top: &LN::CellId) -> Result<(), Self::Error>;
}
