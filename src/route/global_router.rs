// Copyright (c) 2022-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Interface definitions for global routers.

//! Basic trait for a router.

use super::routing_problem::GlobalRoutingProblem;
use crate::{db, prelude::PinAccessOracle};

/// Basic trait for a global router.
pub trait GlobalRouter<LN: db::L2NBase> {
    /// Result of the global routing.
    /// The exact type depends on the routing algorithm.
    type RoutingResult;
    /// Failure during routing.
    type Error;

    /// Get the name of the routing engine.
    fn name(&self) -> String;

    /// Compute the global routes.
    /// Neither layout nor netlist are modified.
    fn route<RP>(&self, routing_problem: &RP) -> Result<Self::RoutingResult, Self::Error>
    where
        RP: GlobalRoutingProblem<LN>;

    /// Compute the global routes with provided pin access locations.
    /// Neither layout nor netlist are modified.
    fn route_with_pin_access_oracle(
        &self,
        routing_problem: &impl GlobalRoutingProblem<LN>,
        _pin_access_oracle: &impl PinAccessOracle<LN>,
    ) -> Result<Self::RoutingResult, Self::Error> {
        // The default implementation ignores the oracle.
        self.route(routing_problem)
    }
}
