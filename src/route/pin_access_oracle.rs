// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! A pin access oracle (PAO) predicts good locations for connecting to pins of standard-cells
//! before the routing is done. This can help guiding the global and especially the detail routing.
//!
//! Usually the pin access is pre-computed for all the pins. The [`PinAccessOracle`] trait then only queries
//! the precomputed results.
//!
//! Not only locations for pin access might be precomputed but also the access directions or the vias
//! which can be used for connecting to the pin without creating design rule violations.

use crate::db;

/// Query interface for precomputed pin access locations.
/// This is intended to guide the router.
pub trait PinAccessOracle<LN>
where
    LN: db::LayoutBase + db::NetlistBase,
{
    /// Get the absolute coordinates and layer for the 'best' access point of the given pin instance.
    fn get_best_pin_access_location(
        &self,
        chip: &LN,
        pin_inst: &LN::PinInstId,
    ) -> (db::Point<LN::Coord>, LN::LayerId) {
        let (location, layer, _maybe_via) =
            self.get_best_pin_access_location_with_via(chip, pin_inst);
        (location, layer)
    }

    /// Get the absolute coordinates, layer and suggested via cell for the 'best' access point of the given pin instance.
    fn get_best_pin_access_location_with_via(
        &self,
        chip: &LN,
        pin_inst: &LN::PinInstId,
    ) -> (db::Point<LN::Coord>, LN::LayerId, Option<LN::CellId>);
}
