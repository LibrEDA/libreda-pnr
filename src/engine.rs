// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Abstraction of algorithm engines such as placer or router engines.

/// Basic trait of an algorithm engine.
pub trait Engine {
    /// Return the name of the engine.
    fn name(&self) -> String;

    /// Try to abort a running engine.
    /// An `Err` might be returned if the engine cannot be aborted.
    fn abort(&self) -> Result<(), ()> {
        Err(())
    }

}
